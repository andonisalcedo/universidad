import java.util.StringTokenizer;
import java.util.Stack;

class Expression
{
	private Nodo root;

	public interface Nodo{
		public float getResultado(SymbolTab syms);
	}


	public class Constante implements Nodo {
		private float a;

		public Constante(float a){
			this.a = a;
		}
		
		public float getResultado(SymbolTab syms){
			return a;
		}

		@Override
		public String toString(){
			return String.valueOf(a);
		}
	}

	public class Variable implements Nodo {
		private String a;

		public Variable(String a){
			this.a = a;
		}

		public float getResultado(SymbolTab syms){
			return syms.get(a);
		}

		@Override
		public String toString(){
			return a;
		}

	}

	public abstract class NodoR implements Nodo {
		protected Nodo a;
		protected Nodo b;
		protected int p;
	
		public NodoR(Nodo a, Nodo b, int p){
			this.a = a;
			this.b = b;
			this.p = p;
		}
		public int getP(){
			return p;
		}
	}
	public class Multiplicacion extends NodoR {
		public Multiplicacion(Nodo a, Nodo b, int p){
			super(a,b,p);
		}
		public float getResultado(SymbolTab syms){
			return a.getResultado(syms) * b.getResultado(syms);
		}
		@Override
		public String toString(){
			if( a instanceof NodoR ){
				NodoR aa = (NodoR)a;
				if( b instanceof NodoR ){
					NodoR bb = (NodoR)b;
					if( aa.getP() > 0 ){
						if( bb.getP() >= 0 ){
							return "("+aa.toString()+") * ("+bb.toString()+")";
						}else{
							return "("+aa.toString()+") * "+bb.toString();
						}
					}else{
						if ( bb.getP() >= 0 ){
							return aa.toString()+" * ("+bb.toString()+")";
						}else{
							return aa.toString()+" * "+bb.toString();
						}
					}
				}else{
					if(aa.getP() > 0 ){
						return "("+aa.toString()+") * "+b.toString();
					}else{
						return aa.toString()+" * "+b.toString();
					}
				}
			}else if( b instanceof NodoR ){
					NodoR bb = (NodoR)b;
					if(bb.getP() >= 0 ){
						return "("+a.toString()+") * "+bb.toString();
					}else{
						return a.toString()+" * "+bb.toString();
					}
			}else{
				return a.toString()+" * "+b.toString();
			}
		}
	}

	public class Division extends NodoR{
		public Division(Nodo a, Nodo b, int p){
			super(a,b,p);
		}
		public float getResultado(SymbolTab syms){
			return a.getResultado(syms) / b.getResultado(syms);
		}
		@Override
		public String toString(){
			if( a instanceof NodoR ){
				NodoR aa = (NodoR)a;
				if( b instanceof NodoR ){
					NodoR bb = (NodoR)b;
					if( aa.getP() > 0 ){
						if( bb.getP() >= 0 ){
							return "("+aa.toString()+") / ("+bb.toString()+")";
						}else{
							return "("+aa.toString()+") / "+bb.toString();
						}
					}else{
						if ( bb.getP() >= 0 ){
							return aa.toString()+" / ("+bb.toString()+")";
						}else{
							return aa.toString()+" / "+bb.toString();
						}
					}
				}else{
					if(aa.getP() > 0 ){
						return "("+aa.toString()+") / "+b.toString();
					}else{
						return aa.toString()+" / "+b.toString();
					}
				}
			}else if( b instanceof NodoR ){
					NodoR bb = (NodoR)b;
					if(bb.getP() >= 0 ){
						return "("+a.toString()+") / "+bb.toString();
					}else{
						return a.toString()+" / "+bb.toString();
					}
			}else{
				return a.toString()+" / "+b.toString();
			}
		}
	}
	public class Suma extends NodoR{
		public Suma(Nodo a, Nodo b, int p){
			super(a,b,p);
		}
		public float getResultado(SymbolTab syms){
			return a.getResultado(syms) + b.getResultado(syms);
		}
		@Override
		public String toString(){
			 if(b instanceof NodoR){
				NodoR bb = (NodoR)b;
				if ( bb.getP() == 1){
					return a.toString()+" + ("+bb.toString()+")";
				}else{
					return a.toString()+" + "+bb.toString();
				}
			}else{
				return a.toString()+" + "+b.toString();
			}
		}
	}
	public class Resta extends NodoR{
		public Resta(Nodo a, Nodo b, int p){
			super(a,b,p);
		}
		public float getResultado(SymbolTab syms){
			return a.getResultado(syms) - b.getResultado(syms);
		}
		@Override
		public String toString(){
			if(b instanceof NodoR){
				NodoR bb = (NodoR)b;
				if ( bb.getP() == 1){
					return a.toString()+" - ("+bb.toString()+")";
				}else{
					return a.toString()+" - "+bb.toString();
				}
			}else{
				return a.toString()+" - "+b.toString();
			}
		}
	}
	
	private void parse(String s)
	{

		Stack<Nodo> stk = new Stack();

		StringTokenizer st = new StringTokenizer(s);

		while (st.hasMoreTokens())
		{
			String tok = st.nextToken();

			if (tok.equals("+"))
			{
				Nodo var1 = stk.pop();
				Nodo var2 = stk.pop();
				Suma resultado = new Suma(var2,var1,1);
				stk.push(resultado);
			}
			else if (tok.equals("-"))
			{
				Nodo var1 = stk.pop();
				Nodo var2 = stk.pop();
				Resta resultado = new Resta(var2,var1,1);
				stk.push(resultado);
			}
			else if (tok.equals("*"))
			{
				Nodo var1 = stk.pop();
				Nodo var2 = stk.pop();
				Multiplicacion resultado = new Multiplicacion(var2,var1,0);
				stk.push(resultado);
			}
			else if (tok.equals("/"))
			{
				Nodo var1 = stk.pop();
				Nodo var2 = stk.pop();
				Division resultado = new Division(var2,var1,0);
				stk.push(resultado);
			}
			else
			{
				if (Character.isLetter(tok.charAt(0)))
				{	
					Variable resultado = new Variable(tok);
					stk.push(resultado);
				}
				else
				{
					float v = Float.valueOf(tok).floatValue();
					Constante resultado = new Constante(v);
					stk.push(resultado);
				}
			}
		}
		root = stk.pop();
	}

	public Expression(String s){
		parse(s);
	}

	public float eval(SymbolTab syms)
	{
		return root.getResultado(syms);
	}

	@Override
	public String toString()
	{
		return  root.toString();
	}
};


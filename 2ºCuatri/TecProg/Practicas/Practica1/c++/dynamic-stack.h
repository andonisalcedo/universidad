#pragma once

template<typename T>
class dynamic_stack {
    private:
        class Nodo {
            public:
                T dato;
                Nodo *next;
                Nodo(){
                }
                Nodo(const T& t){
                    dato = t;
                    next = nullptr;
                }                       
        };
        int total;
        Nodo *first;

	public:
		dynamic_stack(){
			total = 0;
            first = nullptr;
		}
	
		bool push(const T& p){
            Nodo *nuevo = new Nodo;
            if (total != 0){
                nuevo->next = first;
                nuevo->dato = p;
                first = nuevo;
                total++;
            }else{
                nuevo->next = nullptr;
                nuevo->dato = p;
                first = nuevo;
                total = 1;
            }
            return true;
		}
	
		bool pop(){
			bool sePuede = total > 0;
			if (sePuede){ 
				Nodo *aux = new Nodo;
				aux = first;
                first = first->next;
				total--;
				delete aux;
			}
			return sePuede;
		}

	    friend class const_iterator;
	    class const_iterator {
	    	private:
	    		const dynamic_stack<T>& c;
                Nodo *iter;
	    	public:
	    		const_iterator(const dynamic_stack& c_, Nodo *_iter) : c(c_), iter(_iter) {
	    		}

	    		const_iterator& operator++(){ 
                    iter = iter->next;
                    return (*this);
	    	    }

	    		const T& operator*()   const {
	    			return iter->dato;
	    		} 

	    		bool operator!=(const const_iterator& that) const { 
	    			return iter != that.iter;
	    		}
	    };

	    const_iterator begin() const { 
	    	return const_iterator(*this,this->first); 
	    }
	    const_iterator end()   const { 
	    	return const_iterator(*this,nullptr); 
	    }
    

};  




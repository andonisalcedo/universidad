#pragma once

const int MAX = 40;

template<typename T>
class static_stack {
	
	private: 
		T datos[MAX];
		int total;
	public:
	
		static_stack(){
			total = 0;
		}
	
		bool push(const T& p){
			bool sePuede = total < MAX;
			if (sePuede) {
				datos[total] = p;
				total++;
			}
			return sePuede;
		}
	
		bool pop(){
			bool sePuede = total > 0;
			if (sePuede){ 
				total--;
			}
			return sePuede;
		}

	friend class const_iterator;    	
	class const_iterator {
		private:
			int i;
			const static_stack<T>& c;
		public:
			const_iterator(const static_stack& c_, int i_) : i(i_), c(c_) {

			}

			const_iterator& operator++(){ 	 
				i--;
				return (*this);
		    }

			const T& operator*()   const {
				return c.datos[i];
			} 

			bool operator!=(const const_iterator& that) const { 
				return i != that.i;
			}
		};

		const_iterator begin() const { 
			return const_iterator(*this,this->total - 1); 
		}
		const_iterator end()   const { 
			return const_iterator(*this,-1); 
		}
	};

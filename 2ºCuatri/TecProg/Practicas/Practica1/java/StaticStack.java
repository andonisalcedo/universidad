import java.lang.Iterable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.lang.UnsupportedOperationException;


public class StaticStack<T> implements Stack<T>, Iterable<T>{ 

	private static final int MAX = 40; 
	private T[] datos;
	private int total;

	@SuppressWarnings("unchecked")
	public StaticStack()
	{
		datos = (T[])(new Object[MAX]);
		total = 0;
	}

	public boolean push(T t) {
		boolean sePuede = total < MAX;
			if (sePuede) {
				datos[total] = t;
				total++;
			}
			return sePuede;
	}

	public boolean pop(){
		boolean sePuede = total > 0;
		if (sePuede){ 
			total--;
		}
		return sePuede;
	}

	private class StackIterator implements Iterator<T> {

		StaticStack<T> stk;
		int i;

		private StackIterator(StaticStack<T> stk) 
		{
			this.stk = stk;
			i = stk.total - 1;
		}

		public boolean hasNext()	
		{
			return i >= 0;
		}
	

		public T next() throws NoSuchElementException
		{	T t;
			if (!hasNext()){ 
				throw new NoSuchElementException();
			} else {
				t = stk.datos[i];
				i--;
			}
			return t;
		}

		public void remove() throws UnsupportedOperationException
		{
			 throw new UnsupportedOperationException();
		}
	}

	public Iterator<T> iterator()
	{
		return new StackIterator(this);
	}
}
 
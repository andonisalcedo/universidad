import java.lang.Iterable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.lang.UnsupportedOperationException;

public class DynamicStack<T> implements Stack<T>, Iterable<T> {
	
	private Nodo first;
	private int total;

	@SuppressWarnings("unchecked")
	public DynamicStack(){
		first = null;
		total = 0;
	}

	public boolean push(T t) {
		Nodo nuevo = new Nodo(t);
        
		if(first == null){
			first = nuevo;
			total++;

		}else{
			nuevo.next = first;
			first = nuevo;
			total = 1;
		}
		return true;
	}

	public boolean pop(){
		boolean sePuede = total > 0;
		if (sePuede){ 
            first = first.next;
			total--;
		}
		return sePuede;
	}

	public class Nodo{
    	T data;
    	Nodo next;
    	public Nodo(T t){
        	data = t;
            next = null;
        }
	}

	private class StackIterator implements Iterator<T>{
		
		DynamicStack<T> stk;
		Nodo i;
       
	    private StackIterator(DynamicStack<T> stk) {
			this.stk = stk;
			i = stk.first;
		}

		public boolean hasNext(){
			return i != null;
		}

		public T next() throws NoSuchElementException{	
            T t;

			if (!hasNext()){
				 throw new NoSuchElementException();
			} else {
				t = i.data;
				i = i.next;
				
			}
			return t;
		}

		public void remove() throws UnsupportedOperationException{
			throw new UnsupportedOperationException();
		}
	}
	public Iterator<T> iterator(){
		return new StackIterator(this);
	}
}

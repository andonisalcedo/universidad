import java.lang.Iterable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.lang.UnsupportedOperationException;

public interface Stack<T> extends Iterable<T>{ 
		public boolean push(T t);
		public boolean pop();
}
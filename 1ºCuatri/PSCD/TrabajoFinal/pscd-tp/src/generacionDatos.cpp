#include "../include/generacionDatos.hpp"

void InitRanges(){
    ifstream f;
    f.open(Ruta_Datos);
    if (f.is_open()){
        for (size_t i = 0; i < NUM_CIUDADES; i++){
            for (size_t j = 0; j < NUM_CIUDADES; j++){
               int buff;
               f >> buff;
               range[i][j] = buff;
            }
        }
        f.close();
    }else{
        cerr<<"Ha habido un error al abrir el fichero"<<endl;
        exit(0);
    }
}

int DistanceTwoPoints(int idA, int idB){
    return range[idA][idB];
}
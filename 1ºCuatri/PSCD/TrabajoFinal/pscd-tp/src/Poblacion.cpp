#include "../include/Poblacion.hpp"

//CONSTRUCTORES
/**
 *  func:
 *     Poblacion (por defecto)
 *  descripcion:
 *     Inicializa una poblacion vacia con un numero de individuos igual a 0
**/

Poblacion::Poblacion(){
}

/**
 *  func:
 *     Poblacion 
 *  pre:
 *     Se necesita tener inicializado generacionDatos -> range[][]
 *  descripcion:
 *     Crea NUM_IND individuos con un id inicio igual a param@1
**/
Poblacion::Poblacion(int idS){
    //unique_lock<mutex> lck(monitorPob);
    for (size_t i = 0; i < NUM_IND; i++){
        Individuo aux = Individuo(idS);
        poblacion.push_back(aux);
    }
    sCity = idS;
    TotalFitness();
    UpdateProbability();
}
//DESTRUCTOR
Poblacion::~Poblacion(){
}
//GETTERS
/**
 *  func:
 *     getIndividuo 
 *  parm:
 *     id <- int # posicion a la que quieres acceder
 *  pre:
 *     El vector poblacion tiene que tener al menos un tamaño igual a param@1
 *  descripcion:
 *     Devuelve el individuo de la posición id 
**/
Individuo Poblacion::getIndividuo(int id){
    unique_lock<mutex> lck(monitorPob);
    return poblacion[id];
}
/**
 *  func:
 *     getSCity 
 *  pre:
 *     La poblacion tiene que estar inicializada
 *  descripcion:
 *     Devuelve el valor de sCity
**/
int Poblacion::getSCity(){
    unique_lock<mutex> lck(monitorPob);
    return sCity;
}
/**
 *  func:
 *     getNumInd 
 *  descripcion:
 *     Devuelve el tamaño de la poblacion
**/
int Poblacion::getNumInd(){
    unique_lock<mutex> lck(monitorPob);
    return poblacion.size();
}
/**
 *  func:
 *     getTotalFitness
 *  descripcion:
 *     Devuelve el valor de totalFitness
**/
int Poblacion::getTotalFitness(){
    unique_lock<mutex> lck(monitorPob);
    return totalFitness;
}
//METODOS DE CLASE
/**
 *  func:
 *     BestFitness 
 *  pre:
 *     Se necesita tener inicializado generacionDatos -> range[][]
 *     La poblacion tiene que estar inicializada
 *  descripcion:
 *     Devuelve el indice del menor fitness de un individuo de la poblacion
**/
int Poblacion::BestFitness(){
    unique_lock<mutex> lck(monitorPob);
    int best = 0;
    for (size_t i = 1; i < NUM_IND; i++){
        if (poblacion[best].getFitness() > poblacion[i].getFitness()){
            best = i;
        }   
    }
    return best;
}
/**
 *  func:
 *     DeleteInd 
 *  param:
 *     id <- int # Posicion del vector poblacion que se va a eliminar 
 *  pre:
 *     El vector poblacion tiene que tener al menos un tamaño igual a param@1
 *  descripcion:
 *     Elimina poblacion[id]
**/
void Poblacion::DeleteInd(int id){
    unique_lock<mutex> lck(monitorPob);
    poblacion.erase(poblacion.begin()+id);
}
/**
 *  func:
 *     AddInd 
 *  param:
 *     ind <- Individuo # Individuo que se va a añadir al vector
 *  descripcion:
 *     Añade un nuevo Individuo igual a param@1 al final del vector de poblacion
**/
void Poblacion::AddInd(Individuo ind){
    unique_lock<mutex> lck(monitorPob);
    poblacion.push_back(ind);
}
/**
 *  func:
 *     TotalFitness
 *  descripcion:
 *     Actualiza el valor de totalFitness con el sumatorio de todos los fitness de cada Individuo
**/
void Poblacion::TotalFitness(){
    unique_lock<mutex> lck(monitorPob);
    totalFitness = 0;
    for (size_t i = 0; i < poblacion.size(); i++){
        totalFitness += poblacion[i].getFitness();
    }
}
/**
 *  func:
 *     UpdateProbabilidadRWS
 *  descripcion:
 *     Actualiza el valor de cada probablidad de cada individuo de la poblacion
**/
void Poblacion::UpdateProbability(){
    unique_lock<mutex> lck(monitorPob);
    for (size_t i = 0; i < poblacion.size(); i++){
        poblacion[i].setProbability(poblacion[i].getFitness()/totalFitness);
    }
}
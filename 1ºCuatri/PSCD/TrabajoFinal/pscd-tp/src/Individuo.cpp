#include "../include/Individuo.hpp"

//CONSTRUCTORES
/**
 *  func:
 *     Individuo (por defecto)
 *  pre:
 *      Se necesita tener inicilizado generacionDatos->range[][]
 *  descripcion:
 *      Crea un individuo pseudo-aleatorio con un path[0] igual a 0
**/

Individuo::Individuo(){
}

/**
 *  func:
 *     Individuo
 *  params:
 *     id <- int # Id de inicio del individuo de la clase
 *  pre:
 *      Se necesita tener inicilizado generacionDatos->range[][]
 *  descripcion:
 *      Crea un individuo pseudo-aleatorio con un path[0] igual a param@1
**/
Individuo::Individuo(int id){
    
    Init(id);
    FisherYates(id);
    UpdateFitness();
}
//DESTRUCTOR
Individuo::~Individuo(){
}
//METODOS INTERNOS
/**
 *  func:
 *     Init
 *  params:
 *     id <- int # Id de inicio del individuo de la clase
 *  descripcion:
 *     Inicializa la matriz de path con path[0] igual a param@1
**/
void Individuo::Init(int id){

    probability = 0;
    path[0] = id;
    for (size_t i = 1; i < NUM_CIUDADES; i++){
        if(i > id){
            path[i] = i;
        }else{
            path[i] = i-1;
        }
    }
}
//GETTERS
/**
 *  func:
 *     getPath
 *  params:
 *     id <- int # Posicion de path a la que quieres acceder
 *  descripcion:
 *     Devuelve el id de path igual a param@1
**/
int Individuo::getPath(int id){

    return path[id];
}   
/**
 *  func:
 *     getPath
 *  params:
 *     path <- path[] # Array de tamaño NUM_CIUDADES
 *  descripcion:
 *     Actializa el path del individuo con el nuevo path
**/
void Individuo::getPath(int path[NUM_CIUDADES]){

    for(int i = 0; i < NUM_CIUDADES; i++){
        path[i] = this->path[i];
    }
}
/**
 *  func:
 *     getFitness
 *  descripcion:
 *     Devuelve el valor fitness
**/
int Individuo::getFitness(){
    return fitness;
}

double Individuo::getProbability(){
    return probability;
}
//SETTERS
/**
 *  func:
 *     setProbabilidad
 *  param:
 *     pRWS <- double #Nuevo valor de probabilidadRWS
 *  descripcion:
 *     Cambia el valor de probabilidadRWS por param@1
**/
void Individuo::setProbability(double p){
    probability = p;
}

void Individuo::setPath(int path[NUM_CIUDADES]){
     for(int i = 0; i < NUM_CIUDADES; i++){
         this->path[i] = path[i];
     }
 }

void Individuo::setPath(int indice, int id){
    path[indice] = id;
}
//METODOS DE CLASE
/**
 *  func:
 *     UpdateFitness
 *  pre:
 *     Se necesita tener inicializado path <- Init
 *     Se necesita tener inicializado generacionDatos->range[][]
 *  descripcion:
 *     Recorre path actualizando fitness con la distancia total entre id's
**/
void Individuo::UpdateFitness(){
    fitness = 0;
    for (size_t i = 0; i < NUM_CIUDADES; i++){
        fitness += DistanceTwoPoints(i,(i+1) % NUM_CIUDADES);
    } 
}
/**
 *  func:
 *     FisherYates
 *  params:
 *     id <- int # Id de inicio del individuo de la clase
 *  pre:
 *     Se necesita tener inicializado path <- Init
 *  descripcion:
 *     Utiliza el algoritmo Fisher-Yates para desordenar creando 
 *     individuios pseudo-aleatorios
**/
void Individuo::FisherYates(int id){
    int buff;
    int temp;
    for (size_t i = 1; i < NUM_CIUDADES; i++){
        buff = (rand() % i) + 1;
        temp = path[buff];
        path[buff] = path[i];
        path[i] = temp;
    }
}

#ifndef CONSTANTES_HPP
#define CONSTANTES_HPP

#include <string>

const int NUM_CIUDADES = 312;
const int NUM_IND = 200;
const int NUM_GEN = 10000;
const std::string Ruta_Datos = "../datos/usca312_dist.txt";

#endif
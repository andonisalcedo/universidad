#ifndef POBLACION_HPP
#define POBLACION_HPP

#include <vector>
#include "Individuo.hpp"
#include "constantes.hpp"
#include <mutex>
using namespace std;

class Poblacion{
private:
    //ATRIBUTOS
    vector<Individuo> poblacion;
    int sCity;
    int totalFitness;
    //MUTEX
    mutex monitorPob;
public:
    //CONSTRUCTORES
    Poblacion();
    Poblacion(int idS);
    //DESTRUCTOR
    ~Poblacion();
    //GETTERS
    Individuo getIndividuo(int id);
    int getSCity();
    int getNumInd();
    int getTotalFitness();
    //METODOS DE CLASE
    int BestFitness();
    void DeleteInd(int id);
    void AddInd(Individuo ind);
    void TotalFitness();
    void UpdateProbability();
};

#endif
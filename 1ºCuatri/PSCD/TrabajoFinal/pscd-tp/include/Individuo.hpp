#ifndef INDIVIDUO_HPP
#define INDIVIDUO_HPP

#include "constantes.hpp"
#include "generacionDatos.hpp"
using namespace std;

class Individuo{
private:
    //ATRIBUTOS
    int path[NUM_CIUDADES];
    int fitness;
    double probability;
    //METODOS INTERNOS
    void Init(int id);
public:
    //CONSTRUCTORES
    Individuo();
    Individuo(int id);
    //DESTRUCTOR
    ~Individuo();
    //Getters
    int getPath(int id);
    void getPath(int path[NUM_CIUDADES]);
    int getFitness();
    double getProbability();
    //Setters
    void setProbability(double p);
    void setPath(int path[NUM_CIUDADES]);
    void setPath(int indice, int id);
    //METODOS DE CLASE
    void UpdateFitness();
    void FisherYates(int id);
};
#endif

#ifndef GENERACIONDATOS_HPP
#define GENERACIONDATOS_HPP

#include <iostream>
#include <fstream>
#include "constantes.hpp" 

using namespace std;

static int range[NUM_CIUDADES][NUM_CIUDADES]; //Solucionar problema con include 

int DistanceTwoPoints(int idA, int idB);

void InitRanges();

#endif 
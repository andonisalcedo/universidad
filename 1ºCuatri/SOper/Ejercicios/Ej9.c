void alarm_handler(){
    pritf("alarm...");
}

int main(int argc, char* argv[]){
    int fd[2];
    pipe(fd);
    pid_t pid;
    sigset_t mask;
    struct sigacion alarm;
    sigfillset(mask);
    sigdelset(mask,SIGALARM);
    alarm.sa_mask = mask;
    alarm.sa_handler = alarm_handler; 
    sigaction(SIGALARM,alarm);
    switch pid = fork(){
		case -1:
		    perror("fork");
		    exit(1);
		case 0:
		    close(fd[1]);
		    int file = creat("datos.dat",0666);
		    char dato;
		    while(read(fd[0],&dato,sizeof(dato))){
		        int datoE = atoi(dato);
			actua(datoE);
			write(file,datoE,sizeof(datoE);
		    }
		default:
		    close(fd[0]);
		    while(true){
		        int dato = leer_sensor();
			write(fd[1], &dato, sizeof(dato));
			alarm(0);
			sigsuspend();
		    }
	}
}
	   


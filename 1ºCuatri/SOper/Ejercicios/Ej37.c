void alarm_handler(int id){
    signal(SIGALRM,alarm_handler);
}
int main(int argc, char* argv[]){
    int uno[2], dos[2];
    signal(SIGALRM,alarm_handler);
    pipe(uno);
    pipe(dos);
    int i = 0;
    printf("EY\n");   
    switch (fork()){
		case 0:
		    close(uno[1]);
		    close(dos[0]);
		    close(0);
		    dup(uno[0]);
		    close(1);
		    dup(dos[1]);
		    break;
		default:
		    close(uno[0]);
		    close(dos[1]);
		    close(1);
		    dup(uno[1]);
		    close(0);
		    dup(dos[0]);
		    write(1,&i,sizeof(i));
		    break;
	
    }
    while(1){  
	 	read(0,&i,sizeof(i));
	 	alarm(1);
	 	pause();
     	i++;
	 	write(1,&i,sizeof(i));
    }	
 
}


int main(int argc, char* argv[]){
    
    if(argc != 4){
	perror("Numero de parametros invalido");
	exit(1);
    }
	
    pid_t pids[2],pide;
    int fd[2];

    for(int i = 0; i < 2; i++){
        switch pids[i] = fork(){
	    	case 0:
			close(1);
			dup(fd[1]);
			close(fd[0]);
			close(fd[1]);
			execl(argv[i+1],argv[i+1],NULL);
			perror("execl");
			exit(1);
		}
    }

    switch pide = fork(){
		case 0:
		    close(0);
		    dup(fd[0]);
		    close(fd[0]);
		    close(fd[1]);
		    execl(argv[4],argv[4],NULL);
		    perror("execl");
		    exit(1);
	}

    for(int i = 0; i < 3; i++){
		wait(NULL);
    }
} 

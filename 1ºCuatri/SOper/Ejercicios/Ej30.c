﻿void alarm_stop(int id){
    alarm.sa_handler = alarm_start;
    sigaction(SIGINT,alarm,NULL);
    printf(ts.tv_sec);
}
void alarm_start(int id){
    alarm.sa_handler = alarm_stop;
    sigaction(SIGINT,alarm,NULL);
    gettimeofday(ts,NULL);
}
void timeout_handler(int id){
    printf("adios..");
    exit(0);
}
struct timeval *ts;
struct sigaction alarm;
struct sigaction timeout;
int main(int args, char* argv[]){
    sigset_t mask;
    sigfillset(mask);
    sigdelset(mask,SIGALRM);
    sigdelset(mask,SIGINT);
    sigdelset(mask,SIGQUIT);
    alarm.sa_handler = alarm_start;
    alarm.sa_mask = mask;
    sigaction(SIGINT,alarm,NULL);
    sigaddset(mask,SIGINT);
    timeout.sa_handler = timeout_handler;
    timeout.sa_mask = mask;
    sigaction(SIGQUIT,timeout_handler,NULL);
    sigaction(SIGALRM,timeout_handler,NULL);
    alarm(60);
}    

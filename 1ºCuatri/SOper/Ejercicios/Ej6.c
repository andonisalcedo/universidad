﻿int main(int argc, char* argv[]){
    
    if(argc != 3){
	perror("Numero de parametros incorrecto");
	exit(1);
    }
    
    link(argv[1],argv[2]);
    unlink(argv[1]);
}

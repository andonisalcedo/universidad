void child_handler(int id){
	wait(id);
}

int ejecutar(char comando[], int espera){
	struct sigaction child;
	sigset_t = mask;
	sigfillset(mask);
	sigdelset(mask, SIGCHLD);
	child.sa_mask = mask;
	child.sa_handler = child_handler;
	sigaction(SIGCHLD, child, NULL);

	char* aux[];
	aux[0] = strtok(comando," \t");
	for(int i = 1 ; (aux[i] = strtok(NULL," \t")) != NULL; i++)	
	
	switch pid_t pid = fork()
	    case -1:
		perror("fork");
		exit(-1);
	    case 0:
		execv(aux[0], aux, NULL);
		perror("execv");
		exit(1);
	    default:
		if(espera){
		    wait(NULL);
		}
		return pid;
}
